import React, { Component } from 'react';
import axios from 'axios';
// import { Link, Route, BrowserRouter as Router, Switch } from 'react-router-dom';
import '../App.css';
// import Login from './login'
class SignUp extends Component {
    constructor(props) {
        super(props);
        this.state = {
            age: '',
            phonenumber: '',
            name: '',
            // email: '',
            password: ''
        }
    }
    changeEvent(event) {
        // console.log(event.target.name);
        // console.log(event.target.value);
        const name = event.target.name;
        const value = event.target.value;
        this.setState({ [name]: value });
        // console.log(this.state);


    };
    submitEvent(event) {
        event.preventDefault();
        console.log(this.state);
    
axios.post('http://localhost:8080/user/signup',this.state)
.then(function(response) {
  console.log(response);
})
.catch(function(error) {
  console.log(error)
})
};


    render() {
        return (<div className="signup">
            <form onSubmit={this.submitEvent.bind(this)} className="form" >
                <h1>SignUp</h1>
                <hr />
                {/* <label htmlFor="email">Email:-</label><br />
                <input type="email" name="email" id="email" placeholder="enter your email" onChange={this.changeEvent.bind(this)} required /><br /> */}
                {/* <label htmlFor="lastname">Last Name:-</label><br />
                <input type="text" name="lastname" id="lastname" placeholder="enter your last name" onChange={this.changeEvent.bind(this)} required /><br /> */}
                <label htmlFor="name">User Name:-</label><br />
                <input type="text" name="name" id="name" placeholder="enter your name" onChange={this.changeEvent.bind(this)} required /><br />
                <label htmlFor="age">AGE:-</label><br />
                <input type="number" name="age" id="age" placeholder="enter your Age" onChange={this.changeEvent.bind(this)} required /><br />
                <label htmlFor="phonenumber">PhoneNumber:-</label><br />
                <input type="number" name="phonenumber" id="phonenumber" placeholder="enter your phonenumber" onChange={this.changeEvent.bind(this)} required /><br />
                <label htmlFor="password">Password:-</label><br />
                <input type="password" name="password" placeholder="enter your password" id="password" onChange={this.changeEvent.bind(this)} required /><br />
                <input type="submit" value="SignUp" id="signupBtn"  />
                <hr />
            </form>
                {/* <Router>
                    <Link to='/login'><span>click here for Login</span></Link>
                    <Switch>
                        <Route  exact strict path='/login'  component={Login}/>
                    </Switch>
                </Router> */}
        </div>);
    }
}

export default SignUp;