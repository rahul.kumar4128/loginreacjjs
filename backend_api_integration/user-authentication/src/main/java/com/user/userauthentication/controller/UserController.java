package com.user.userauthentication.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.user.userauthentication.repository.UserRepository;
import com.user.userauthentication.user.User;

@CrossOrigin(origins = "http://localhost:3000")
@RestController
@RequestMapping("/user")
public class UserController {

	@Autowired
	private UserRepository repo;

	@PostMapping("/adduser")
	public String addUser(@RequestBody List<User> users) {
		repo.saveAll(users);
		return users.size() + " user added.";
	}

	@GetMapping("/getuser")
	public String findByUsernameAndPassword(@RequestParam String username,@RequestParam String password){
		List<User> user= repo.findByUsernameAndPassword(username, password);
		String isPresent="";
		if(user.isEmpty()) {
			isPresent="Invalid username or password";
		}else {
			isPresent="Login successful";
		}
		return isPresent;
	}

}
